package com.girnarsoft.training.exceptions;

import java.util.Scanner;

public class ExceptionDemo {
	public static void main(String[] args) {
		try {
			System.out.println(getUserChoice());
		} catch (NegativeNumberException nne) {
//			System.out.println(nne.getMessage());
			nne.printStackTrace();
		}
	}
	
	private static int getUserChoice() throws NegativeNumberException {
		System.out.println("Please enter a positive number: ");
		Scanner scanner = new Scanner(System.in);
		int userInput = scanner.nextInt();
		if(userInput < 0) {
			throw new NegativeNumberException("Negative number given");
		}
		
		return userInput;
	}
}
