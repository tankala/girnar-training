package com.girnarsoft.training.exceptions;

public class NegativeNumberException extends NumberFormatException {
	public NegativeNumberException(String message) {
		super(message);
	}
}
