package com.girnarsoft.training.collections;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import com.girnarsoft.training.oops.beans.BankAccount;

public class MapExample {

	private static final Scanner SCANNER = new Scanner(System.in);
	private static final String MASTER_KEY = "weroiuweoriweoiur";

	public static void main(String[] args) {
		int userChoice = 0;
		Map<Integer, BankAccount> bankAccounts = new TreeMap<>();
		while (((userChoice = getUserChoice()) <= 3) && userChoice >= 1) {
			userAction(userChoice, bankAccounts);
		}
		SCANNER.close();
	}

	private static int getUserChoice() {
		System.out.println("What do you want to do?");
		System.out.println("1 for bank account creation");
		System.out.println("2 for bank accounts list");
		System.out.println("3 for bank account search");
		System.out.println("Anything else for exit");
		return SCANNER.nextInt();
	}

	private static BankAccount getNewBackAccount(Map<Integer, BankAccount> bankAccounts) {
		int accountId = 0;
		do {
			System.out.println("Please enter AccountId");
			accountId = SCANNER.nextInt();
		} while (bankAccounts.containsKey(accountId));
		System.out.println("Please enter pin: ");
		int pin = SCANNER.nextInt();
		return new BankAccount(pin, accountId);
	}

	private static void listBankAccounts(Map<Integer, BankAccount> bankAccounts) {
		for (BankAccount bankAccount : bankAccounts.values()) {
			printBankAccountDetails(bankAccount);
		}
	}

	private static void printBankAccountDetails(BankAccount bankAccount) {
		System.out.println("Account No: " + bankAccount.getAccountId());
		System.out.println("Balance: " + bankAccount.getBankBalance(MASTER_KEY));
	}

	private static void userAction(int userChoice, Map<Integer, BankAccount> bankAccounts) {
		switch (userChoice) {
		case 1:
			BankAccount bankAccount = getNewBackAccount(bankAccounts);
			bankAccounts.put(bankAccount.getAccountId(), bankAccount);
			break;
		case 2:
			listBankAccounts(bankAccounts);
			break;
		case 3:
			searchAndPrintBankAccount(bankAccounts);
			break;
		default:
			System.out.println("Something wrong");
			break;
		}
	}

	private static void searchAndPrintBankAccount(Map<Integer, BankAccount> bankAccounts) {
		System.out.println("Account Id: ");
		int accountId = SCANNER.nextInt();
		BankAccount bankAccount = bankAccounts.get(accountId);
		if (bankAccount != null) {
			printBankAccountDetails(bankAccount);
		} else {
			System.out.println("no account found");
		}
	}
}
