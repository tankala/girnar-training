package com.girnarsoft.training.collections;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.girnarsoft.training.oops.beans.BankAccount;

public class ArrayListExec {
	
	private static final Scanner SCANNER = new Scanner(System.in);
	private static final String MASTER_KEY = "weroiuweoriweoiur";
	
	public static void main(String[] args) {
		int userChoice = 0;
		List<BankAccount> bankAccounts = new ArrayList<>();
		while(((userChoice = getUserChoice()) <= 2) && userChoice >= 1) {
			userAction(userChoice, bankAccounts);
		}
	}
	
	private static int getUserChoice() {
		System.out.println("What do you want to do?");
		System.out.println("1 for bank account creation");
		System.out.println("2 for bank accounts list");
//		System.out.println("3 for bank account search");
		System.out.println("Anything else for exit");
		return SCANNER.nextInt();
	}
	
	private static BankAccount getNewBackAccount(int accountId) {
		System.out.println("Please enter pin: ");
		int pin = SCANNER.nextInt();
		return new BankAccount(pin, accountId);
	}
	
	private static void listBankAccounts(List<BankAccount> bankAccounts) {
		for (BankAccount bankAccount : bankAccounts) {
			System.out.println("Account No: " + bankAccount.getAccountId());
			System.out.println("Balance: " + bankAccount.getBankBalance(MASTER_KEY));
		}
	}
	
	private static void userAction(int userChoice, List<BankAccount> bankAccounts) {
		switch (userChoice) {
		case 1:
			bankAccounts.add(getNewBackAccount(bankAccounts.size() + 1));
			break;
		case 2:
			listBankAccounts(bankAccounts);
			break;
		default:
			System.out.println("Something wrong");
			break;
		}
	}
}
