package com.girnarsoft.training.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class SiteReader {
	public static void main(String[] args) {
		System.out.println(getHTML("https://www.cardekho.com/india-car-news/volkswagen-ameo-polo-and-vento-sport-edition-launched-21971.htm"));
	}

	private static String getHTML(String urlToRead) {
		StringBuilder result = new StringBuilder();
		BufferedReader bufferedReader = null;
		PrintWriter writer = null;
		try {
			URL url = new URL(urlToRead);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			writer = new PrintWriter(new File("/Users/ashoktankala/Documents/workspace-sts/girnar-training/check.html"));
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				result.append(line + "\n");
				writer.println(line);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(writer != null) {
				writer.close();
			}
		}
		return result.toString();
	}
}
