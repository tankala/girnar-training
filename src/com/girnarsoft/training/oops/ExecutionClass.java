package com.girnarsoft.training.oops;

import java.util.HashMap;
import java.util.Map;

import com.girnarsoft.training.oops.beans.BankAccount;
import com.girnarsoft.training.oops.beans.SavingsAccount;

public class ExecutionClass {
	public static void main(String[] args) {
		Map<Integer, BankAccount> bankAccountsMap = getBankAccountsMap();
		
		//Deposit
		BankAccount bankAccount = bankAccountsMap.get(123456);
		bankAccount.deposit(1000);
		bankAccount = bankAccountsMap.get(123457);
		bankAccount.deposit(1000);
		
		//Withdraw
		bankAccount = bankAccountsMap.get(123456);
		System.out.println("Withdraw Status: " + bankAccount.withdraw(1234, 1000));
		bankAccount = bankAccountsMap.get(123457);
		System.out.println("Withdraw Status: " + bankAccount.withdraw(1234, 1000));
	}
	
	private static Map<Integer, BankAccount> getBankAccountsMap() {
		Map<Integer, BankAccount> bankAccountsMap = new HashMap<>();
		BankAccount savingsAccount = new SavingsAccount(1234, 123456, 500);
		bankAccountsMap.put(savingsAccount.getAccountId(), savingsAccount);
		BankAccount bankAccount1 = new BankAccount(1234, 123457);
		bankAccountsMap.put(bankAccount1.getAccountId(), bankAccount1);
		
		return bankAccountsMap;
	}
}
