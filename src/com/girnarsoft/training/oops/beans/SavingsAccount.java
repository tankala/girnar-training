package com.girnarsoft.training.oops.beans;

public class SavingsAccount extends BankAccount {

	public SavingsAccount(int pin, int accountId, int bankBalance) {
		super(pin, accountId);
		super.bankBalance = bankBalance;
	}
	
	public boolean withdraw(int pin, double withdrawAmount) {
		if(super.verifyPin(pin) && (bankBalance >= withdrawAmount + 500)) {			
			bankBalance -= withdrawAmount;
			return true;
		} else {
			return false;
		}
	}
	
	public boolean withdraw(String masterKey, double withdrawAmount) {
		if((BankAccount.MASTER_KEY.equals(masterKey)) && (bankBalance >= withdrawAmount + 500)) {			
			bankBalance -= withdrawAmount;
			return true;
		} else {
			return false;
		}
	}
}
