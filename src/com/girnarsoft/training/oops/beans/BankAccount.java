package com.girnarsoft.training.oops.beans;

public class BankAccount {
	private int pin;
	protected double bankBalance;
	private int accountId;
	protected static final String MASTER_KEY = "weroiuweoriweoiur";
	
	public BankAccount(int pin, int accountId) {
		this.pin = pin;
		this.accountId = accountId;
		this.bankBalance = 0;
	}
	
	public boolean deposit(double depostAmount) {
		bankBalance += depostAmount;
		return true;
	}
	
	public boolean withdraw(int pin, double withdrawAmount) {
		if((this.pin == pin) && (bankBalance >= withdrawAmount)) {			
			bankBalance -= withdrawAmount;
			return true;
		} else {
			return false;
		}
	}
	
	public boolean withdraw(String masterKey, double withdrawAmount) {
		if((BankAccount.MASTER_KEY.equals(masterKey)) && (bankBalance >= withdrawAmount)) {			
			bankBalance -= withdrawAmount;
			return true;
		} else {
			return false;
		}
	}

	public boolean verifyPin(int pin) {
		if(this.pin == pin) {
			return true;
		} else {
			return false;
		}
	}

	public void setPin(int oldPin, int newPin) {
		if(this.pin == oldPin) {
			this.pin = newPin;
		}
	}

	public double getBankBalance(int pin) {
		if(this.pin == pin) {
			return bankBalance;
		} else {
			return 0.0;
		}
	}
	
	public double getBankBalance(String masterKey) {
		if(MASTER_KEY.equals(masterKey)) {
			return bankBalance;
		} else {
			return 0.0;
		}
	}

	public int getAccountId() {
		return accountId;
	}
}
