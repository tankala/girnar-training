package com.girnarsoft.training.threads;

public class ThreadExecution {
	public static void main(String[] args) throws InterruptedException {
		for(int count = 0; count < 5; count++) {
			SubThread subThread = new SubThread(count + 1);
			Thread thread = new Thread(subThread);
			thread.start();
		}
	}
}
