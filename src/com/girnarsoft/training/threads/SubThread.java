package com.girnarsoft.training.threads;

public class SubThread implements Runnable {
	
	int threadNumber;
	
	public SubThread(int threadNumber) {
		this.threadNumber = threadNumber;
	}

	@Override
	public void run() {
//		try {
//			Thread.sleep(1000);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
		System.out.println("Thread number: " + threadNumber);
	}

}
